import React, { useState, useEffect } from 'react';
import {
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,
    Image,
    TouchableOpacity,
    Button
    } from 'react-native';
import {
    Colors
    } from 'react-native/Libraries/NewAppScreen';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faInfo, faUser } from '@fortawesome/free-solid-svg-icons'
import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import contactService from '../contactService/contactService';

const MainScreen = ({ navigation }) => {
    const [usersDefault, setUsersDefault] = useState([]);
    const [contacts, setContacts] = useState(usersDefault);
    const isDarkMode = useColorScheme() === 'dark';
    
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
        flex: 1,
        alignItems: "center"
    };

    useEffect(async () => {
        if ( usersDefault.length === 0 ) {
            const contacts = await contactService.getAll();
            setUsersDefault(contacts);
            setContacts(contacts);
        }
    })
    
    const onDeleteContact = async (id) => {
        const newContacts = await contactService.delete(id);
        setUsersDefault(newContacts);
        setContacts(newContacts);
    }

    const onAddContact = async (newUser) => {
        newUser.id = uuidv4();
        const newContacts = await contactService.add(newUser);
        setUsersDefault(newContacts);
        setContacts(newContacts);
    }

    const filterContacts = (text) => {
        if (text) {
            const filteredContacts = usersDefault.filter(contact => contact.username.includes(text));
            setContacts(filteredContacts);
        } else {
            setContacts(usersDefault);
        }
    }

    const renderIcon = (url) => {
            if (url.match(/^http[^\?]*.(jpg|jpeg|gif|png|tiff|bmp)(\?(.*))?$/gmi)) {
                return <Image
                        source={{
                            uri: url,
                        }}
                        style={{ width: 40, height: 40, marginRight: 10}}
                    />;
            } else {
                return <View>
                            <FontAwesomeIcon icon={ faUser } size={ 40 } color={ "grey" } style={{ marginRight: 10 }} />
                        </View>;
            }
    }

    return (usersDefault.length !== 0 ? 
        <View style={backgroundStyle}>
            <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={backgroundStyle}>
                <View styles={styles.container}>
                    <TextInput
                    style={styles.input}
                    onChangeText={text => filterContacts(text)}
                    placeholder="Search...">
                    </TextInput>
                    <Button
                        title="Add contact"
                        style={{ marginTop: 10 }}
                        onPress={() => {navigation.navigate('New Contact', { onAdd: onAddContact })}} 
                        >
                    </Button>
                    {contacts.map(contact => 
                        <TouchableOpacity key={contact.id} onPress={() => {navigation.navigate('Profile', { user: contact, onDelete: onDeleteContact })}} >
                            <View style={styles.card}>
                                <View style={{flexDirection: "row"}}>
                                    {renderIcon(contact.avatar)}
                                    <View style={{ flexDirection: "column" }}>
                                        <Text>{contact.username}</Text>
                                        <Text>{contact.email}</Text>
                                    </View>
                                </View>
                                <View>
                                    <FontAwesomeIcon icon={ faInfo } style={{ width: 15, height: 15 }} />
                                </View>
                            </View>
                        </TouchableOpacity>
                     )}
                </View>
            </ScrollView>
        </View>
        :
        <View style={styles.container}>
            <Text style={styles.loading}>Loading...</Text>
        </View>
    );
};
 
const styles = StyleSheet.create({
   input: {
     borderWidth: 1,
     borderRadius: 5,
     padding: 5,
     width: 300,
     marginTop: 15,
     marginBottom: 15
   },
   container: {
     flex: 1,
     alignItems: "center",
     backgroundColor: '#ecf0f1',
     padding: 8,
     marginLeft: 20
   },
   card: {
     flexDirection: "row",
     borderWidth: 1,
     width: 300,
     padding: 5,
     marginTop: 15,
     marginBottom: 15,
     justifyContent: "space-between",
     alignItems: "center",
     borderRadius: 5
   },
   loading: {
    marginTop: 15,
   }
});
 
 export default MainScreen;
 