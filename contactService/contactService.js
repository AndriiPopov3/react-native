import contacts from '../database';

class Contact {
    constructor(contacts) {
        this.db = contacts;
    }

    async getAll() {
        return this.db;
    }

    async add(contact) {
        const newContacts = this.db.concat(contact);
        this.db = newContacts;
        return this.db;
    }

    async delete(id) {
        const newContacts = this.db.filter(contact => contact.id !== id);
        this.db = newContacts;
        return this.db;
    }
}

const contactService = new Contact(contacts);
export default contactService;