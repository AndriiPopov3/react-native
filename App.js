import 'react-native-gesture-handler';
import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from './components/mainScreen';
import ProfileScreen from './components/profileScreen';
import NewContactScreen from './components/newContactScreen';


const Stack = createStackNavigator();

const App = () => {

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Phone List"
          component={MainScreen}
        />
        <Stack.Screen name="Profile" component={ProfileScreen} />
        <Stack.Screen name="New Contact" component={NewContactScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
