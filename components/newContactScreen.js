import React, { useState } from 'react';
import {
    StyleSheet,
    View,
    TextInput,
    Button,
    Alert,
    } from 'react-native';

const NewContactScreen = ({ navigation, route }) => {
    const [username, setUsername] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [email, setEmail] = useState("");
    const [avatar, setAvatar] = useState("");

    const validatePhone = (phone) => {
        if(!phone.startsWith("+") || !/^(3|38[0-9]{0,11})$/.test(phone.slice(1))) {
            Alert.alert("Error: Invalid phone format", "Must start from +38 and only contain numbers");
            return false;
        } else {
            return true;
        }
    }

    const validateEmail = (email) => {
        if (!email.includes("@")) {
            Alert.alert("Error: Invalid email format", "Must contain @");
            return false;
        } else {
            return true;
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.mainBlock}>
                <TextInput
                    style={styles.input}
                    onChangeText={text => setUsername(text)}
                    placeholder="Username"
                    textContentType="name">
                </TextInput>
                <TextInput
                    style={styles.input}
                    onChangeText={text => setPhoneNumber(text)}
                    placeholder="Phone Number"
                    keyboardType="phone-pad"
                    textContentType="telephoneNumber"
                    onEndEditing={() => validatePhone(phoneNumber)}
                >
                </TextInput>
                <TextInput
                    style={styles.input}
                    onChangeText={text => setEmail(text)}
                    placeholder="Email"
                    keyboardType="email-address"
                    textContentType="emailAddress"
                    onEndEditing={() => validateEmail(email)}>
                </TextInput>
                <TextInput
                    style={styles.input}
                    onChangeText={text => setAvatar(text)}
                    placeholder="Avatar"
                    textContentType="URL">
                </TextInput>
                <Button title="Add Contact" style={{ marginTop: 10,  }} onPress={() => {
                        if (username !== "" &&
                        email !== "" &&
                        phoneNumber !== "" &&
                        avatar !== "") {
                            if (validatePhone(phoneNumber) && validateEmail(email)) {
                                route.params.onAdd({ username, phoneNumber, email, avatar });
                                navigation.navigate('Phone List');
                            }
                        } else {
                            Alert.alert("Error: Invalid contact info", "Fields must not be empty");
                        }      
                    }} ></Button>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    mainBlock: {
        width: "80%",
        alignItems: "center",
    },
    textContainer: {
        flexDirection: "column",
        alignItems: "center",
        width: "80%",
    },
    input: {
        borderWidth: 1,
        borderRadius: 5,
        padding: 5,
        width: "80%",
        marginTop: 10,
        marginBottom: 10
    },
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: '#ecf0f1',
        padding: 8,
        marginLeft: 20
    },
    card: {
        flexDirection: "row",
        borderWidth: 1,
        width: "80%",
        padding: 5,
        marginTop: 30,
        justifyContent: "space-between",
        alignItems: "center",
        borderRadius: 5
    }
});

export default NewContactScreen;
  