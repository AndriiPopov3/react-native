import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Button,
    Linking,
    TouchableOpacity
    } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'

const ProfileScreen = ({ navigation, route }) => {
    const { id, avatar, username, phoneNumber } = route.params.user;

    const renderIcon = (url) => {
        if (url.match(/^http[^\?]*.(jpg|jpeg|gif|png|tiff|bmp)(\?(.*))?$/gmi)) //if(statusText == OK)
        {
            return <Image
                    source={{
                        uri: url,
                    }}
                    style={{ width: 100, height: 100, marginTop: 10}}
                />;
        } else {
            return <View>
                        <FontAwesomeIcon icon={ faUser } size={ 100 } color={ "grey" } style={{ marginTop: 10 }} />
                    </View>
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.mainBlock}>
                <TouchableOpacity>
                    {renderIcon(avatar)}
                </TouchableOpacity>
                <View style={styles.textContainer}>
                    <Text style={{ marginTop: 10, padding: 10 }}>Contact name: {username}</Text>
                    <Text style={{ marginTop: 10, padding: 10 }}>Phone Number: {phoneNumber}</Text>
                </View>
                <View style={{flexDirection: "row", justifyContent: "space-between", width: "60%"}}>
                    <Button title="Call" style={{ margin: 10 }} onPress={()=> Linking.openURL(`tel:${phoneNumber}`)} ></Button>
                    <Button title="Delete" style={{ marginTop: 10 }} onPress={() => {
                        route.params.onDelete(id);
                        navigation.navigate('Phone List');
                    }} ></Button>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    mainBlock: {
        width: "80%",
        alignItems: "center",
    },
    textContainer: {
        flexDirection: "column",
        alignItems: "center",
        width: "80%",
    },
    input: {
        borderWidth: 1,
        borderRadius: 5,
        padding: 5,
        width: "80%",
        marginTop: 20
    },
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
    card: {
        flexDirection: "row",
        borderWidth: 1,
        width: "80%",
        padding: 5,
        marginTop: 30,
        justifyContent: "space-between",
        alignItems: "center",
        borderRadius: 5
    }
});
 
 export default ProfileScreen;
 