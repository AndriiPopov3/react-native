export default [
    {
        id: "1",
        username: "test_username",
        email: "test_email@gmail.com",
        avatar: "https://www.metal-archives.com/images/1/3/0/130.jpg?4210",
        phoneNumber: "+381234567890"
    },
    {
        id: "2",
        username: "templarFlame",
        email: "deusvult@gmail.com",
        avatar: "https://www.metal-archives.com/images/5/8/6/7/586745.jpg?5732",
        phoneNumber: "+380483059284"
    },
    {
        id: "3",
        username: "hinokami_kagura",
        email: "refers@gmail.com",
        avatar: "https://www.metal-archives.com/images/3/9/9/0/399075.jpg?4122",
        phoneNumber: "+380194029430"
    },
    {
        id: "4",
        username: "stillwater_nitro",
        email: "test_email@gmail.com",
        avatar: "https://www.metal-archives.com/images/6/4/9/4/649484.jpg?2836",
        phoneNumber: "+1039495302394"
    },
    {
        id: "5",
        username: "user_to_delete",
        email: "test_email2@gmail.com",
        avatar: "https://www.metal-archives.com/images/1/3/0/130.jpg?4210",
        phoneNumber: "+1038593904930"
    }
]